# Copyright(c) 2018 by craftyguy "Clayton Craft" <clayton@craftyguy.net>
# Distributed under GPLv3+ (see LICENSE) WITHOUT ANY WARRANTY.

import machine
import main
import net_config
import network
import sys
import ujson
import uos
import utime
import webrepl
from lib import LED

DEBUG_PIN = 5       # If this pin is pulled low, then app will not start
STATUS_LED_PIN = 14
PWR_LED_PIN = 13        # Turns on immediately, pin should be pulled low
RESET_CONFIG_PIN = 12
CONFIG_FILE = "settings.json"

webrepl.start()
status_led = LED(STATUS_LED_PIN)
debug_pin = machine.Pin(DEBUG_PIN, machine.Pin.IN)
reset_config = machine.Pin(RESET_CONFIG_PIN, machine.Pin.IN).value()
pwr_led = LED(PWR_LED_PIN)
reset_config = machine.Pin(RESET_CONFIG_PIN, machine.Pin.IN).value()
pwr_led.on()


def make_client(essid='esp-motiondetect', passphrase='',
                hostname='esp-motiondetect'):
    s_if = network.WLAN(network.STA_IF)
    a_if = network.WLAN(network.AP_IF)
    if a_if.active():
        a_if.active(False)
    if not s_if.isconnected():
        s_if.active(True)
        s_if.config(dhcp_hostname="esp-motiondetect")
        # Connect to Wifi.
        s_if.connect(essid, passphrase)
        while not s_if.isconnected():
            pass
        print("Wifi connected: ", s_if.ifconfig())


def make_station(essid='esp-motiondetect', passphrase=''):
    s_if = network.WLAN(network.STA_IF)
    s_if.active(False)
    a_if = network.WLAN(network.AP_IF)
    a_if.active(False)
    a_if.active(True)
    old_essid = a_if.config('essid')
    # Channel chosen by fair roll of the dice
    a_if.config(essid=essid, channel=4)
    if passphrase:
        # Use WPA2/PSK
        a_if.config(authmode=3, password=passphrase)
    else:
        # Open network (e.g. for debug or net config)
        a_if.config(authmode=0)
    # Full reset required for a new essid to be applied
    if essid != old_essid:
        machine.reset()


def network_config_wizard(config_file):
    make_station('esp-motiondetect')
    global status_led
    status_led.on()
    net_config.start(config_file)
    machine.reset()


if debug_pin.value():
    make_station('esp-motiondetect')
    print("Debug mode is enabled, dropping to REPL!")
    status_led.pulse(200, 4)
    sys.exit(0)

if reset_config:
    status_led.pulse(100, 5)
    # Create a new (or clear an existing) config file
    # This is so that a reset before new settings are
    # provided takes us back to the net config wizard
    with open(CONFIG_FILE, 'w') as f:
        f.write('')
    network_config_wizard(CONFIG_FILE)

try:
    with open(CONFIG_FILE, 'r') as f:
        config = ujson.loads(f.read())
except (OSError, ValueError):
    network_config_wizard(CONFIG_FILE)

try:
    net_mode = config['net_mode']
    net_essid = config['net_essid']
    net_pass = config['net_pass']
    if net_mode == 'client':
        net_hostname = config['net_hostname']
except ValueError:
    network_config_wizard(CONFIG_FILE)

status_led.pulse(200, 2)
status_led.on()
if net_mode == 'ap':
    make_station(net_essid, net_pass)
else:
    make_client(net_essid, net_pass, net_hostname)
# Delay 2 seconds for things to settle
utime.sleep(2)
status_led.off()

repl_uart = machine.UART(0, 115200)

try:
    uos.dupterm(None, 1)
    main.run()
except Exception as e:
    uos.dupterm(repl_uart, 1)
    sys.print_exception(e)
finally:
    # Always re-attach repl to uart(0)
    uos.dupterm(repl_uart, 1)
