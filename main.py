# Copyright(c) 2018 by craftyguy "Clayton Craft" <clayton@craftyguy.net>
# Distributed under GPLv3+ (see LICENSE) WITHOUT ANY WARRANTY.

import binascii
import machine
import sys
import utime
from lib import LED
from ws_connection import ClientClosedError
from ws_server import WebSocketClient
from ws_multiserver import WebSocketMultiServer

SPEED_OF_SOUND = 29.1           # Speed of sound (cm/us)
TRIG_PIN = 15
ECHO_PIN = 12
STATUS_LED_PIN = 14


def exp_smooth(cur, prev, t):
    return cur * t + (1 - t) * prev


class US_100_Ultrasonic_Sensor:
    def __init__(self):
        self.uart = machine.UART(0)
        self.uart.init(9600)
        # max range of device in cm
        self.max_range = 300

    def get_distance(self):
        high = None
        low = None
        distance = 0
        while distance <= 0 or distance >= self.max_range:
            """ Get distance in centimeters """
            self.uart.write(b'\x55')
            utime.sleep_ms(20)
            while(not self.uart.any()):
                continue
            try:
                high = self.uart.read(1)
                low = self.uart.read(1)
            except TypeError:
                continue
            if not high or not low:
                continue
            high = int(binascii.hexlify(high), 16)
            low = int(binascii.hexlify(low), 16)
            distance = ((high * 256) + low) / 10
        return distance

    def get_distances(self, num_samples, rate):
        distances = []
        for i in range(num_samples):
            utime.sleep_ms(rate)
            d = self.get_distance()
            if d:
                distances.append(d)
        return distances


class Client(WebSocketClient):
    def __init__(self, conn):
        super().__init__(conn)
        # self.dist_sensor = (TRIG_PIN, ECHO_PIN)
        self.dist_sensor = US_100_Ultrasonic_Sensor()
        self.sample_duration = None
        self.prev_distance = 0.0
        self.start_ms = 0
        self.rate = 60
        self.exp_smooth_t = 1/8
        self.status_led = LED(STATUS_LED_PIN)
        self.sampling = False

    def process(self):
        self.status_led.set(self.sampling)
        try:
            if self.sampling:
                cur_ms = utime.ticks_ms()
                if self.sample_duration:
                    if utime.ticks_diff(self.sample_duration,
                                        cur_ms) <= 0:
                        self.sampling = False
                        print('Done sampling')
                        self.connection.write('done')
                if self.sampling:
                    distances = []
                    distance = self.dist_sensor.get_distance()
                    # Set initial smoothed value by taking samples and
                    # smoothing them
                    if not self.prev_distance:
                        distances = self.dist_sensor.get_distances(5,
                                                                   self.rate)
                        for i in range(len(distances)):
                            if i == 0:
                                continue
                            distance = exp_smooth(distances[i],
                                                  distances[i-1],
                                                  self.exp_smooth_t)
                        self.prev_distance = distance
                    if distance:
                        exp_smooth(distance, self.prev_distance,
                                   self.exp_smooth_t)
                        self.prev_distance = distance
                    else:
                        distance = 0
                    self.connection.write('%f;%f' % (
                        utime.ticks_diff(cur_ms, self.start_ms) / 1000,
                        distance / 100))
                    utime.sleep_ms(self.rate)
            # Process any new messages
            msg = self.connection.read()
            if not msg:
                return
            msg = msg.decode("utf-8")
            # "start_sampling;rate=sample_rate;duration;sample_duration);"
            if msg.startswith("start_sampling"):
                self.params = {}
                p = msg.split(';')
                # first param is start_sampling
                print(p)
                for param in p:
                    if '=' in param:
                        name, value = param.split('=')
                        if name not in self.params.keys():
                            self.params[name] = value
                self.rate = 50
                if 'duration' in self.params.keys():
                    self.sample_duration = utime.ticks_add(
                            utime.ticks_ms(), int(self.params['duration']))
                    self.start_ms = utime.ticks_ms()
                else:
                    self.sample_duration = 0
                self.sampling = True
            elif msg == "stop_sampling":
                self.sampling = False
            else:
                self.connection.write("unknown message received!")
        except ClientClosedError:
            self.sampling = False
            self.connection.close()


class Server(WebSocketMultiServer):
    def __init__(self):
        super().__init__("main.html", 1)
        # super().__init__("main.html.gz", 2)

    def _make_client(self, conn):
        return Client(conn)


def run():
    server = Server()
    server.start()
    try:
        while True:
            server.process_all()
    except KeyboardInterrupt:
        pass
    server.stop()
    sys.exit(0)



