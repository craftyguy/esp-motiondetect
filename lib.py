# Copyright(c) 2018 by craftyguy "Clayton Craft" <clayton@craftyguy.net>
# Distributed under GPLv3+ (see LICENSE) WITHOUT ANY WARRANTY.

import machine
import utime


class LED:
    def __init__(self, pin):
        self.led = machine.Pin(pin, machine.Pin.OUT)
        self.off()

    def pulse(self, duration_ms, times=1):
        for i in range(times):
            self.on()
            utime.sleep_ms(duration_ms)
            self.off()
            utime.sleep_ms(duration_ms)

    def toggle(self):
        self.led.set(self.get ^ 1)

    def on(self):
        self.set(1)

    def off(self):
        self.set(0)

    def set(self, state=0):
        self.led.value(state)

    def get(self):
        return self.led.value()




