# Copyright(c) 2018 by craftyguy "Clayton Craft" <clayton@craftyguy.net>
# Distributed under GPLv3+ (see LICENSE) WITHOUT ANY WARRANTY.

import ujson
from ws_connection import ClientClosedError
from ws_server import WebSocketClient
from ws_multiserver import WebSocketMultiServer

get_config = True
config_data = ""


class Client(WebSocketClient):
    def __init__(self, conn):
        super().__init__(conn)

    def process(self):
        global get_config
        global config_data
        # Process any new messages
        try:
            msg = self.connection.read()
        except ClientClosedError:
            return
        if not msg:
            return
        try:
            ujson.loads(msg)
        except ValueError:
            self.connection.write('failure parsing json from msg: %s'
                                  % msg)
            # msg was not valid json..
            return
        print(msg)
        self.connection.write('success')
        config_data = msg


class Server(WebSocketMultiServer):
    def __init__(self):
        super().__init__("net_config.html", 8)

    def _make_client(self, conn):
        return Client(conn)


def start(config_file):
    """ Start network config client
    config_file - is a file to write config to (will be overwritten) """
    server = Server()
    server.start()
    try:
        while not config_data:
            server.process_all()
    except KeyboardInterrupt:
        pass
    server.stop()
    with open(config_file, 'w') as f:
        f.write(config_data)
